{
	"appTitle": "Fotos",
	"translator": "luckkmaxx",
	"@translator": {
		"description": "Name of the translator(s) for this language"
	},
	"photosTabLabel": "Fotos",
	"@photosTabLabel": {
		"description": "Label of the tab that lists user photos"
	},
	"collectionsTooltip": "Colecciones",
	"@collectionsTooltip": {
		"description": "Groups of photos, e.g., albums, trash bin, etc"
	},
	"zoomTooltip": "Zoom",
	"@zoomTooltip": {
		"description": "Tooltip of the zoom button"
	},
	"refreshMenuLabel": "Actualizar",
	"@refreshMenuLabel": {
		"description": "Label of the refresh entry in menu"
	},
	"settingsMenuLabel": "Ajustes",
	"@settingsMenuLabel": {
		"description": "Label of the settings entry in menu"
	},
	"selectionAppBarTitle": "{count} seleccionado",
	"@selectionAppBarTitle": {
		"description": "Title of the contextual app bar that shows number of selected items",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"addSelectedToAlbumSuccessNotification": "Selección añadida a {album} con éxito",
	"@addSelectedToAlbumSuccessNotification": {
		"description": "Inform user that the selected items are added to an album successfully",
		"placeholders": {
			"album": {
				"example": "Sunday Walk"
			}
		}
	},
	"addSelectedToAlbumFailureNotification": "No se pudo añadir selección al álbum",
	"@addSelectedToAlbumFailureNotification": {
		"description": "Inform user that the selected items cannot be added to an album"
	},
	"addToAlbumTooltip": "Añadir a álbum",
	"@addToAlbumTooltip": {
		"description": "Add selected items to an album"
	},
	"addToAlbumSuccessNotification": "Archivo añadido a {album} con éxito",
	"@addToAlbumSuccessNotification": {
		"description": "Inform user that the item is added to an album successfully",
		"placeholders": {
			"album": {
				"example": "Sunday Walk"
			}
		}
	},
	"addToAlbumFailureNotification": "Error al añadir al álbum",
	"@addToAlbumFailureNotification": {
		"description": "Inform user that the item cannot be added to an album"
	},
	"addToAlbumAlreadyAddedNotification": "El elemento ya está en el álbum",
	"@addToAlbumAlreadyAddedNotification": {
		"description": "Inform user that the item is already in the album"
	},
	"deleteSelectedProcessingNotification": "{count, plural, =1{Borrando 1 elemento} other{Borrando {count} elementos}}",
	"@deleteSelectedProcessingNotification": {
		"description": "Inform user that the selected items are being deleted",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"deleteSelectedSuccessNotification": "Elementos borrados con éxito",
	"@deleteSelectedSuccessNotification": {
		"description": "Inform user that the selected items are deleted successfully"
	},
	"deleteSelectedFailureNotification": "{count, plural, =1{Error al borrar 1 elemento} other{Error al borrar {count} elementos}}",
	"@deleteSelectedFailureNotification": {
		"description": "Inform user that some/all the selected items cannot be deleted",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"archiveTooltip": "Archivar",
	"@archiveTooltip": {
		"description": "Archive selected items"
	},
	"archiveSelectedProcessingNotification": "{count, plural, =1{Archivando 1 elemento} other{Archivando {count} archivos}}",
	"@archiveSelectedProcessingNotification": {
		"description": "Archiving the selected items",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"archiveSelectedSuccessNotification": "Archivado con éxito",
	"@archiveSelectedSuccessNotification": {
		"description": "Archived all selected items successfully"
	},
	"archiveSelectedFailureNotification": "{count, plural, =1{Error al archivar 1 elemento} other{Error al archivar {count} elementos}}",
	"@archiveSelectedFailureNotification": {
		"description": "Cannot archive some of the selected items",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"unarchiveTooltip": "Desarchivar selección",
	"@unarchiveTooltip": {
		"description": "Unarchive selected items"
	},
	"unarchiveSelectedProcessingNotification": "{count, plural, =1{Desarchivando 1 elemento} other{Desarchivando {count} elementos}}",
	"@unarchiveSelectedProcessingNotification": {
		"description": "Unarchiving selected items",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"unarchiveSelectedSuccessNotification": "Selección desarchivada con éxito",
	"@unarchiveSelectedSuccessNotification": {
		"description": "Unarchived all selected items successfully"
	},
	"unarchiveSelectedFailureNotification": "{count, plural, =1{Error al desarchivar 1 elemento} other{Error al desarchivar {count} elementos}}",
	"@unarchiveSelectedFailureNotification": {
		"description": "Cannot unarchive some of the selected items",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"deleteTooltip": "Eliminar",
	"@deleteTooltip": {
		"description": "Delete selected items"
	},
	"deleteProcessingNotification": "Eliminando elemento",
	"@deleteProcessingNotification": {
		"description": "Inform user that the item is being deleted"
	},
	"deleteSuccessNotification": "Elemento borrado en éxito",
	"@deleteSuccessNotification": {
		"description": "Inform user that the item is deleted successfully"
	},
	"deleteFailureNotification": "Error al borrar elemento",
	"@deleteFailureNotification": {
		"description": "Inform user that the item cannot be deleted"
	},
	"removeSelectedFromAlbumTooltip": "Quitar selección del álbum",
	"@removeSelectedFromAlbumTooltip": {
		"description": "Tooltip of the button that remove selected items from an album"
	},
	"removeSelectedFromAlbumSuccessNotification": "{count, plural, =1{1 elemento quitado del álbum} other{{count} elementos quitados del álbum}}",
	"@removeSelectedFromAlbumSuccessNotification": {
		"description": "Inform user that the selected items are removed from an album successfully",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"removeSelectedFromAlbumFailureNotification": "Error al quitar elementos del álbum",
	"@removeSelectedFromAlbumFailureNotification": {
		"description": "Inform user that the selected items cannot be removed from an album"
	},
	"addServerTooltip": "Añadir servidor",
	"@addServerTooltip": {
		"description": "Tooltip of the button that adds a new server"
	},
	"removeServerSuccessNotification": "Quitado {server} con éxito",
	"@removeServerSuccessNotification": {
		"description": "Inform user that a server is removed successfully",
		"placeholders": {
			"server": {
				"example": "http://www.example.com"
			}
		}
	},
	"createAlbumTooltip": "Crear nuevo álbum",
	"@createAlbumTooltip": {
		"description": "Tooltip of the button that creates a new album"
	},
	"createAlbumFailureNotification": "Error al crear álbum",
	"@createAlbumFailureNotification": {
		"description": "Inform user that an album cannot be created"
	},
	"albumSize": "{count, plural, =0{Empty} =1{1 elemento} other{{count} elementos}}",
	"@albumSize": {
		"description": "Number of items inside an album",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"albumArchiveLabel": "Archivo",
	"@albumArchiveLabel": {
		"description": "Archive"
	},
	"connectingToServer": "Conectando a\n{server}",
	"@connectingToServer": {
		"description": "Inform user that the app is connecting to a server",
		"placeholders": {
			"server": {
				"example": "http://www.ejemplo.com"
			}
		}
	},
	"nameInputHint": "Nombre",
	"@nameInputHint": {
		"description": "Hint of the text field expecting name data"
	},
	"albumNameInputInvalidEmpty": "Introduce un nombre para el álbum",
	"@albumNameInputInvalidEmpty": {
		"description": "Inform user that the album name input field cannot be empty"
	},
	"skipButtonLabel": "SALTAR",
	"@skipButtonLabel": {
		"description": "Label of the skip button"
	},
	"confirmButtonLabel": "CONFIRMAR",
	"@confirmButtonLabel": {
		"description": "Label of the confirm button"
	},
	"signInHeaderText": "Iniciar sesión en Nextcloud",
	"@signInHeaderText": {
		"description": "Inform user what to do in sign in widget"
	},
	"signIn2faHintText": "*Usa una contraseña de aplicación si tienes activada en el servidor la autentificación en dos pasos.",
	"@signIn2faHintText": {
		"description": "Notify users with 2FA enabled what should be done in order to sign in correctly"
	},
	"serverAddressInputHint": "Dirección del servidor",
	"@serverAddressInputHint": {
		"description": "Hint of the text field expecting server address data"
	},
	"serverAddressInputInvalidEmpty": "Introduce la dirección del servidor",
	"@serverAddressInputInvalidEmpty": {
		"description": "Inform user that the server address input field cannot be empty"
	},
	"usernameInputHint": "Usuario",
	"@usernameInputHint": {
		"description": "Hint of the text field expecting username data"
	},
	"usernameInputInvalidEmpty": "Introduce tu nombre de usuario",
	"@usernameInputInvalidEmpty": {
		"description": "Inform user that the username input field cannot be empty"
	},
	"passwordInputHint": "Contraseña",
	"@passwordInputHint": {
		"description": "Hint of the text field expecting password data"
	},
	"passwordInputInvalidEmpty": "Introduce tu contraseña",
	"@passwordInputInvalidEmpty": {
		"description": "Inform user that the password input field cannot be empty"
	},
	"rootPickerHeaderText": "Marca las carpetas que serán añadidas",
	"@rootPickerHeaderText": {
		"description": "Inform user what to do in root picker widget"
	},
	"rootPickerSubHeaderText": "Solo se mostrará el contenido de las carpetas marcadas. Toca SALTAR para incluir todas",
	"@rootPickerSubHeaderText": {
		"description": "Inform user what to do in root picker widget"
	},
	"rootPickerNavigateUpItemText": "(atrás)",
	"@rootPickerNavigateUpItemText": {
		"description": "Text of the list item to navigate up the directory tree"
	},
	"rootPickerUnpickFailureNotification": "Error al desmarcar elemento",
	"@rootPickerUnpickFailureNotification": {
		"description": "Failed while unpicking an item in the root picker list"
	},
	"rootPickerListEmptyNotification": "Por favor, selecciona al menos una carpeta o toca SALTAR para seleccionar todas",
	"@rootPickerListEmptyNotification": {
		"description": "Error when user pressing confirm without picking any folders"
	},
	"setupWidgetTitle": "Empecemos",
	"@setupWidgetTitle": {
		"description": "Title of the introductory widget"
	},
	"setupSettingsModifyLaterHint": "Puedes cambiar esto luego en Ajustes",
	"@setupSettingsModifyLaterHint": {
		"description": "Inform user that they can modify this setting after the setup process"
	},
	"setupHiddenPrefDirNoticeDetail": "Está aplicación crea una carpeta en Nextcloud para guardar las preferencias. Por favor no la modifiques o elimines a no ser que quieras desinstalar ésta aplicación.",
	"@setupHiddenPrefDirNoticeDetail": {
		"description": "Inform user about the preference folder created by this app"
	},
	"settingsWidgetTitle": "Ajustes",
	"@settingsWidgetTitle": {
		"description": "Title of the Settings widget"
	},
	"settingsLanguageTitle": "Idioma",
	"@settingsLanguageTitle": {
		"description": "Set display language"
	},
	"settingsExifSupportTitle": "Soporte EXIF",
	"@settingsExifSupportTitle": {
		"description": "Title of the EXIF support setting"
	},
	"settingsExifSupportTrueSubtitle": "Requiere un uso mayor de la red",
	"@settingsExifSupportTrueSubtitle": {
		"description": "Subtitle of the EXIF support setting when the value is true. The goal is to warn user about the possible side effects of enabling this setting"
	},
	"settingsViewerTitle": "Visualizador",
	"settingsViewerDescription": "Personalizar el visualizador de imágenes/vídeos",
	"settingsViewerPageTitle": "Ajustes de visualizador",
	"@settingsViewerPageTitle": {
		"description": "Dedicated page for viewer settings"
	},
	"settingsScreenBrightnessTitle": "Brillo de pantalla",
	"settingsScreenBrightnessDescription": "Invalidar nivel de brillo del sistema",
	"settingsForceRotationTitle": "Ignorar autorotación desactivada",
	"settingsForceRotationDescription": "Rotar la pantalla incluso si la autorotación está desactivada",
	"settingsThemeTitle": "Tema",
	"settingsThemeDescription": "Personaliza la apariencia de la aplicación",
	"settingsThemePageTitle": "Ajustes de tema",
	"@settingsThemePageTitle": {
		"description": "Dedicated page for theme settings"
	},
	"settingsFollowSystemThemeTitle": "Usar el mismo tema que android",
	"@settingsFollowSystemThemeTitle": {
		"description": "Respect the system dark mode settings introduced on Android 10"
	},
	"settingsUseBlackInDarkThemeTitle": "Tema más oscuro",
	"@settingsUseBlackInDarkThemeTitle": {
		"description": "Make the dark theme darker"
	},
	"settingsUseBlackInDarkThemeTrueDescription": "Usar negro con el tema oscuro",
	"@settingsUseBlackInDarkThemeTrueDescription": {
		"description": "When black in dark theme is set to true"
	},
	"settingsUseBlackInDarkThemeFalseDescription": "Usar gris oscuro con el tema oscuro",
	"@settingsUseBlackInDarkThemeFalseDescription": {
		"description": "When black in dark theme is set to false"
	},
	"settingsAboutSectionTitle": "Acerca de",
	"@settingsAboutSectionTitle": {
		"description": "Title of the about section in settings widget"
	},
	"settingsVersionTitle": "Versión",
	"@settingsVersionTitle": {
		"description": "Title of the version data item"
	},
	"settingsSourceCodeTitle": "Código fuente",
	"@settingsSourceCodeTitle": {
		"description": "Title of the source code item"
	},
	"settingsBugReportTitle": "Comunicar un problema",
	"@settingsBugReportTitle": {
		"description": "Report issue"
	},
	"settingsCaptureLogsTitle": "Capturar logs",
	"@settingsCaptureLogsTitle": {
		"description": "Capture app logs for bug report"
	},
	"settingsCaptureLogsDescription": "Ayuda al desarrollador a diagnosticar fallos",
	"settingsTranslatorTitle": "Traductor",
	"@settingsTranslatorTitle": {
		"description": "Title of the translator item"
	},
	"writePreferenceFailureNotification": "Error al establecer preferencias",
	"@writePreferenceFailureNotification": {
		"description": "Inform user that the preference file cannot be modified"
	},
	"enableButtonLabel": "HABILITAR",
	"@enableButtonLabel": {
		"description": "Label of the enable button"
	},
	"exifSupportDetails": "Habilitando el soporte EXIF podrás ver metadatos como la fecha de captura, modelo de cámara, etc. Para leer estos metadatos, se necesita un uso extra de la red para obtener la imagen en tamaño original. La aplicación sólo empezará a descargar cuando esté conectado a una red wifi.",
	"@exifSupportDetails": {
		"description": "Detailed description of the exif support feature"
	},
	"exifSupportConfirmationDialogTitle": "¿Habilitar soporte EXIF?",
	"@exifSupportConfirmationDialogTitle": {
		"description": "Title of the dialog to confirm enabling exif support"
	},
	"captureLogDetails": "Para obtener logs para reportar fallos:\n\n1. Habilita ésta opción\n2. Reproduce el fallo\n3. Desabilita ésta opción\n4. Busca nc-photos.log en la carpeta de descargas\n\n*Si el fallo provoca que la aplicación se cierre, no se puede capturar el log. En ese caso, por favor contacta con el desarrollador para más instrucciones",
	"@captureLogDetails": {
		"description": "Detailed description on capturing logs"
	},
	"captureLogSuccessNotification": "Logs guargados con éxito",
	"@captureLogSuccessNotification": {
		"description": "Captured logs are successfully saved to the download directory"
	},
	"doneButtonLabel": "LISTO",
	"@doneButtonLabel": {
		"description": "Label of the done button"
	},
	"nextButtonLabel": "SIGUIENTE",
	"@nextButtonLabel": {
		"description": "Label of the next button"
	},
	"connectButtonLabel": "CONECTAR",
	"@connectButtonLabel": {
		"description": "Label of the connect button"
	},
	"rootPickerSkipConfirmationDialogContent": "Se incluirán todos tus archivos",
	"@rootPickerSkipConfirmationDialogContent": {
		"description": "Inform user what happens after skipping root picker"
	},
	"megapixelCount": "{count}MP",
	"@megapixelCount": {
		"description": "Resolution of an image in megapixel",
		"placeholders": {
			"count": {
				"example": "1.3"
			}
		}
	},
	"secondCountSymbol": "{count}s",
	"@secondCountSymbol": {
		"description": "Number of seconds",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"millimeterCountSymbol": "{count}mm",
	"@millimeterCountSymbol": {
		"description": "Number of millimeters",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"detailsTooltip": "Detalles",
	"@detailsTooltip": {
		"description": "Tooltip of the details button"
	},
	"downloadTooltip": "Descargar",
	"@downloadTooltip": {
		"description": "Tooltip of the download button"
	},
	"downloadProcessingNotification": "Descargando archivo",
	"@downloadProcessingNotification": {
		"description": "Inform user that the file is being downloaded"
	},
	"downloadSuccessNotification": "Descargado con éxito",
	"@downloadSuccessNotification": {
		"description": "Inform user that the file is downloaded successfully"
	},
	"downloadFailureNotification": "Error al descargar",
	"@downloadFailureNotification": {
		"description": "Inform user that the file cannot be downloaded"
	},
	"downloadFailureNoPermissionNotification": "Se requiere permiso de acceso al almacenamiento",
	"@downloadFailureNoPermissionNotification": {
		"description": "Inform user that the file cannot be downloaded due to missing storage permission"
	},
	"nextTooltip": "Siguiente",
	"@nextTooltip": {
		"description": "Tooltip of the next button"
	},
	"previousTooltip": "Anterior",
	"@previousTooltip": {
		"description": "Tooltip of the previous button"
	},
	"webSelectRangeNotification": "Mantén Shift + click para seleccionar todo entre medias",
	"@webSelectRangeNotification": {
		"description": "Inform web user how to select items in range"
	},
	"mobileSelectRangeNotification": "Mantenga presionado otro elemento para seleccionar todo entre ellos",
	"@mobileSelectRangeNotification": {
		"description": "Inform mobile user how to select items in range"
	},
	"updateDateTimeDialogTitle": "Modificar fecha y hora",
	"@updateDateTimeDialogTitle": {
		"description": "Dialog to modify the date & time of a file"
	},
	"dateSubtitle": "Fecha",
	"timeSubtitle": "Hora",
	"dateYearInputHint": "Año",
	"dateMonthInputHint": "Mes",
	"dateDayInputHint": "Día",
	"timeHourInputHint": "Hora",
	"timeMinuteInputHint": "Minuto",
	"timeSecondInputHint": "Segundo",
	"dateTimeInputInvalid": "Valor inválido",
	"@dateTimeInputInvalid": {
		"description": "Invalid date/time input (e.g., non-numeric characters)"
	},
	"updateDateTimeFailureNotification": "Error al modificar fecha y hora",
	"@updateDateTimeFailureNotification": {
		"description": "Failed to set the date & time of a file"
	},
	"albumDirPickerHeaderText": "Selecciona las carpetas a asociar con el álbum",
	"@albumDirPickerHeaderText": {
		"description": "Pick the folders for a folder based album"
	},
	"albumDirPickerSubHeaderText": "Sólo el contenido de las carpetas asociadas se incluirán en éste álbum",
	"@albumDirPickerSubHeaderText": {
		"description": "Pick the folders for a folder based album"
	},
	"albumDirPickerListEmptyNotification": "Por favor selecciona al menos una carpeta",
	"@albumDirPickerListEmptyNotification": {
		"description": "Error when user pressing confirm without picking any folders"
	},
	"createAlbumDialogBasicLabel": "Álbum simple",
	"@createAlbumDialogBasicLabel": {
		"description": "Basic album"
	},
	"createAlbumDialogBasicDescription": "El álbum simple organiza el contenido de los elementos que vayas añadiendo, sin importar de las carpetas del servidor que procedan",
	"@createAlbumDialogBasicDescription": {
		"description": "Describe what a basic album is"
	},
	"createAlbumDialogFolderBasedLabel": "Álbum de carpetas",
	"@createAlbumDialogFolderBasedLabel": {
		"description": "Folder based album"
	},
	"createAlbumDialogFolderBasedDescription": "Los álbumes de carpetas muestran el contenido de las carpetas que elijas",
	"@createAlbumDialogFolderBasedDescription": {
		"description": "Describe what a folder based album is"
	},
	"convertBasicAlbumMenuLabel": "Convertir en álbum simple",
	"@convertBasicAlbumMenuLabel": {
		"description": "Convert a non-basic album to a basic one"
	},
	"convertBasicAlbumConfirmationDialogTitle": "Convertir en álbum simple",
	"@convertBasicAlbumConfirmationDialogTitle": {
		"description": "Make sure the user wants to perform the conversion"
	},
	"convertBasicAlbumConfirmationDialogContent": "El contenido de éste álbum ya no será gestionado por la aplicación segun el contenido de las carpetas asociadas. Podrás añadir o eliminar elementos libremente.\n\nÉsta conversión es irreversible",
	"@convertBasicAlbumConfirmationDialogContent": {
		"description": "Make sure the user wants to perform the conversion"
	},
	"convertBasicAlbumSuccessNotification": "Álbum convertido con éxito",
	"@convertBasicAlbumSuccessNotification": {
		"description": "Successfully converted the album"
	},
	"importFoldersTooltip": "Importar carpetas",
	"@importFoldersTooltip": {
		"description": "Menu entry in the album page to import folders as albums"
	},
	"albumImporterHeaderText": "Importar carpetas como álbumes",
	"@albumImporterHeaderText": {
		"description": "Import folders as albums"
	},
	"albumImporterSubHeaderText": "Las carpetas sugeridas se alistan a continuación. Dependiendo de la cantidad de archivos en tu servidor, esto podría tardar un rato en terminar.",
	"@albumImporterSubHeaderText": {
		"description": "Import folders as albums"
	},
	"importButtonLabel": "IMPORTAR",
	"albumImporterProgressText": "Importando carpetas",
	"@albumImporterProgressText": {
		"description": "Message shown while importing"
	},
	"editAlbumMenuLabel": "Editar álbum",
	"@editAlbumMenuLabel": {
		"description": "Edit the opened album"
	},
	"doneButtonTooltip": "Listo",
	"editTooltip": "Editar",
	"editAccountConflictFailureNotification": "Ya existe una cuenta con los mismos ajustes",
	"@editAccountConflictFailureNotification": {
		"description": "Error when user modified an account such that it's identical to another one"
	},
	"genericProcessingDialogContent": "Por favor, espera",
	"@genericProcessingDialogContent": {
		"description": "Generic dialog shown when the app is temporarily blocking user input to work on something"
	},
	"sortTooltip": "Ordenar",
	"sortOptionDialogTitle": "Ordenar por",
	"@sortOptionDialogTitle": {
		"description": "Select how the photos should be sorted"
	},
	"sortOptionTimeAscendingLabel": "Antiguos primero",
	"@sortOptionTimeAscendingLabel": {
		"description": "Sort by time, in ascending order"
	},
	"sortOptionTimeDescendingLabel": "Nuevos primero",
	"@sortOptionTimeDescendingLabel": {
		"description": "Sort by time, in descending order"
	},
	"sortOptionAlbumNameLabel": "Nombre del álbum",
	"@sortOptionAlbumNameLabel": {
		"description": "Sort by album name, in ascending order"
	},
	"sortOptionAlbumNameDescendingLabel": "Nombre del álbum (inverso)",
	"@sortOptionAlbumNameDescendingLabel": {
		"description": "Sort by album name, in descending order"
	},
	"albumEditDragRearrangeNotification": "Mantén presionado y arrastra un elemento para reordenarlo manualmente",
	"@albumEditDragRearrangeNotification": {
		"description": "Instructions on how to rearrange photos"
	},
	"albumAddTextTooltip": "Añadir texto",
	"@albumAddTextTooltip": {
		"description": "Add some text that display between photos to an album"
	},
	"shareTooltip": "Compartir",
	"@shareTooltip": {
		"description": "Share selected items to other apps"
	},
	"shareSelectedEmptyNotification": "Selecciona algunas fotos para compartir",
	"@shareSelectedEmptyNotification": {
		"description": "Shown when user pressed the share button with only non-sharable items (e.g., text labels) selected"
	},
	"shareDownloadingDialogContent": "Descargando",
	"@shareDownloadingDialogContent": {
		"description": "Downloading photos to be shared"
	},
	"searchTooltip": "Buscar",
	"albumSearchTextFieldHint": "Buscar álbumes",
	"clearTooltip": "Limpiar",
	"@clearTooltip": {
		"description": "Clear some sort of user input, typically a text field"
	},
	"listNoResultsText": "Sin resultados",
	"@listNoResultsText": {
		"description": "When there's nothing in a list"
	},
	"listEmptyText": "Vacío",
	"@listEmptyText": {
		"description": "When there's nothing in a list"
	},
	"albumTrashLabel": "Papelera",
	"@albumTrashLabel": {
		"description": "Deleted photos"
	},
	"restoreTooltip": "Restaurar",
	"@restoreTooltip": {
		"description": "Restore selected items from trashbin"
	},
	"restoreSelectedProcessingNotification": "{count, plural, =1{Restaurando 1 elemento} other{Restaurando {count} elementos}}",
	"@restoreSelectedProcessingNotification": {
		"description": "Restoring selected items from trashbin",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"restoreSelectedSuccessNotification": "Restaurado todo con éxito",
	"@restoreSelectedSuccessNotification": {
		"description": "Restored all selected items from trashbin successfully"
	},
	"restoreSelectedFailureNotification": "{count, plural, =1{Fallo al restaurar 1 elemento} other{Fallo al restaurar {count} elementos}}",
	"@restoreSelectedFailureNotification": {
		"description": "Cannot restore some of the selected items from trashbin",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"restoreProcessingNotification": "Restaurando éste elemento",
	"@restoreProcessingNotification": {
		"description": "Restoring the opened item from trashbin"
	},
	"restoreSuccessNotification": "Elemento restaurado con éxito",
	"@restoreSuccessNotification": {
		"description": "Restored the opened item from trashbin successfully"
	},
	"restoreFailureNotification": "Fallo al restaurar éste elemento",
	"@restoreFailureNotification": {
		"description": "Cannot restore the opened item from trashbin"
	},
	"deletePermanentlyTooltip": "Eliminar permanentemente",
	"@deletePermanentlyTooltip": {
		"description": "Permanently delete selected items from trashbin"
	},
	"deletePermanentlyConfirmationDialogTitle": "Eliminar PERMANENTEMENTE",
	"@deletePermanentlyConfirmationDialogTitle": {
		"description": "Make sure the user wants to delete the items"
	},
	"deletePermanentlyConfirmationDialogContent": "Los elementos seleccionados se eliminarán permanentemente del servidor.\n\nÉsta acción es irreversible!",
	"@deletePermanentlyConfirmationDialogContent": {
		"description": "Make sure the user wants to delete the items"
	},
    "albumSharedLabel": "Compartido",
	"@albumSharedLabel": {
		"description": "A small label placed next to a shared album"
	},
	"setAlbumCoverProcessingNotification": "Estableciendo foto como portada del álbum",
	"@setAlbumCoverProcessingNotification": {
		"description": "Setting the opened item as the album cover"
	},
	"setAlbumCoverSuccessNotification": "Portada establecida",
	"@setAlbumCoverSuccessNotification": {
		"description": "Set the opened item as the album cover successfully"
	},
	"setAlbumCoverFailureNotification": "Fallo al establecer portada",
	"@setAlbumCoverFailureNotification": {
		"description": "Cannot set the opened item as the album cover"
	},
	"metadataTaskProcessingNotification": "Procesando metadatos en segundo plano",
	"@metadataTaskProcessingNotification": {
		"description": "Shown when the app is reading image metadata"
	},
	"metadataTaskPauseNoWiFiNotification": "Esperando WiFi",
	"@metadataTaskPauseNoWiFiNotification": {
		"description": "Shown when the app has paused reading image metadata"
	},
	"configButtonLabel": "CONFIGURAR",
	"useAsAlbumCoverTooltip": "Usar como portada del álbum",
	"helpTooltip": "Ayuda",
	"removeFromAlbumTooltip": "Quitar del álbum",
	"@removeFromAlbumTooltip": {
		"description": "Remove the opened photo from an album"
	},	
	"changelogTitle": "Registro de cambios",
	"@changelogTitle": {
		"description": "Title of the changelog dialog"
	},
	"serverCertErrorDialogTitle": "No se puede confiar en el certificado del servidor",
	"@serverCertErrorDialogTitle": {
		"description": "Title of the dialog to warn user about an untrusted SSL certificate"
	},
	"serverCertErrorDialogContent": "El servidor podría ser hackeado o alguien podría rondar su información",
	"@serverCertErrorDialogContent": {
		"description": "Warn user about an untrusted SSL certificate"
	},
	"advancedButtonLabel": "AVANZADO",
	"@advancedButtonLabel": {
		"description": "Label of the advanced button"
	},
	"whitelistCertDialogTitle": "¿Poner en lista blanca certificado desconocido?",
	"@whitelistCertDialogTitle": {
		"description": "Title of the dialog to let user decide whether to whitelist an untrusted SSL certificate"
	},
	"whitelistCertDialogContent": "Puedes poner en lista blanca el certificado para hacer que la aplicación lo acepte. ADVERTENCIA: Esto supone un gran riesgo de seguridad. Asegúrate de que el certificado está firmado por ti mismo o una entidad de confianza\n\nHost: {host}\nFingerprint: {fingerprint}",
	"@whitelistCertDialogContent": {
		"description": "Let user decide whether to whitelist an untrusted SSL certificate",
		"placeholders": {
			"host": {
				"example": "www.ejemplo.com"
			},
			"fingerprint": {
				"example": "da39a3ee5e6b4b0d3255bfef95601890afd80709"
			}
		}
	},
	"whitelistCertButtonLabel": "ASUMIR RIESGO Y PONER EN LISTA BLANCA",
	"@whitelistCertButtonLabel": {
		"description": "Label of the whitelist certificate button"
	},
	"fileSharedByDescription": "Compartido contigo por éste usuario",
	"@fileSharedByDescription": {
		"description": "The app will show the owner of the file if it's being shared with you by others. The name of the owner is displayed above this line"
	},
	"emptyTrashbinTooltip": "Vaciar papelera",
	"@emptyTrashbinTooltip": {
		"description": "Permanently delete all items from trashbin"
	},
	"emptyTrashbinConfirmationDialogTitle": "Vaciar papelera",
	"@emptyTrashbinConfirmationDialogTitle": {
		"description": "Make sure the user wants to delete all items"
	},
	"emptyTrashbinConfirmationDialogContent": "Todos los elementos se eliminarán permanentemente del servidor.\n\nÉsta acción es irreversible",
	"@emptyTrashbinConfirmationDialogContent": {
		"description": "Make sure the user wants to delete all items"
	},
	"unsetAlbumCoverTooltip": "Quitar portada",
	"@unsetAlbumCoverTooltip": {
		"description": "Unset the cover of the opened album"
	},
	"unsetAlbumCoverProcessingNotification": "Quitando portada",
	"@unsetAlbumCoverProcessingNotification": {
		"description": "Unsetting the cover of the opened album"
	},
	"unsetAlbumCoverSuccessNotification": "Portada quitada con éxito",
	"@unsetAlbumCoverSuccessNotification": {
		"description": "Unset the cover of the opened album successfully"
	},
	"unsetAlbumCoverFailureNotification": "Error al quitar portada",
	"@unsetAlbumCoverFailureNotification": {
		"description": "Cannot unset the cover of the opened album"
	},
	"muteTooltip": "Silenciar",
	"@muteTooltip": {
		"description": "Mute the video player"
	},
	"unmuteTooltip": "No silenciar",
	"@unmuteTooltip": {
		"description": "Unmute the video player"
	},
	"collectionPeopleLabel": "Gente",
	"@collectionPeopleLabel": {
		"description": "Browse photos grouped by person"
	},
	"personPhotoCountText": "{count, plural, =1{1 foto} other{{count} fotos}}",
	"@personPhotoCountText": {
		"description": "Number of photos associated to a specific person",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"slideshowTooltip": "Presentación de diapositivas",
	"@slideshowTooltip": {
		"description": "A button to start a slideshow from the current collection"
	},
	"slideshowSetupDialogTitle": "Configurar diapositivas",
	"@slideshowSetupDialogTitle": {
		"description": "Setup slideshow before starting"
	},
	"slideshowSetupDialogDurationTitle": "Duración de cada imagen (MM:SS)",
	"@slideshowSetupDialogDurationTitle": {
		"description": "Set the duration of each image in MM:SS format. This setting is ignored for videos"
	},
	"slideshowSetupDialogShuffleTitle": "Aleatorias",
	"@slideshowSetupDialogShuffleTitle": {
		"description": "Whether to shuffle the collection"
	},
	"slideshowSetupDialogRepeatTitle": "Repetir",
	"@slideshowSetupDialogRepeatTitle": {
		"description": "Whether to restart the slideshow from the beginning after the last slide"
	},
	"linkCopiedNotification": "Enlace copiado",
	"@linkCopiedNotification": {
		"description": "Copied the share link to clipboard"
	},
	"shareMethodDialogTitle": "Compartir como",
	"@shareMethodDialogTitle": {
		"description": "Let the user pick how they want to share"
	},
	"shareMethodFileTitle": "Archivo",
	"@shareMethodFileTitle": {
		"description": "Share the actual file"
	},
	"shareMethodFileDescription": "Descarga el archivo y compártelo con otras apps",
	"shareMethodPublicLinkTitle": "Enlace público",
	"@shareMethodPublicLinkTitle": {
		"description": "Create a share link on server and share it"
	},
	"shareMethodPublicLinkDescription": "Crea en el servidor un nuevo enlace público. Cualquiera con el enlace puede acceder al archivo",
	"shareMethodPasswordLinkTitle": "Enlace protegido con contraseña",
	"@shareMethodPasswordLinkTitle": {
		"description": "Create a password protected share link on server and share it"
	},
	"shareMethodPasswordLinkDescription": "Crea en el servidor un nuevo enlace protegido con contraseña",

	"errorUnauthenticated": "Acceso sin identificación. Por favor inicia sesión otra vez si el problema continúa.",
	"@errorUnauthenticated": {
		"description": "Error message when server responds with HTTP401"
	},
	"errorDisconnected": "No se pudo conectar. El servidor no está en línea o tu dispositivo está desconectado.",
	"@errorDisconnected": {
		"description": "Error message when the app can't connect to the server"
	},
	"errorLocked": "Archivo bloqueado en el servidor. Inténtalo de nuevo más tarde.",
	"@errorLocked": {
		"description": "Error message when server responds with HTTP423"
	},
	"errorInvalidBaseUrl": "No se pudo comunicar. Asegúrate que la dirección es la URL base de tu Nextcloud.",
	"@errorInvalidBaseUrl": {
		"description": "Error message when the base URL is invalid"
	},
	"errorWrongPassword": "No se pudo autentificar. Verifica tu usuario y contraseña.Unable to authenticate.",
	"@errorWrongPassword": {
		"description": "Error message when the username or password is wrong"
	},
	"errorServerError": "Error del servidor. Asegúrate que el servidor está configurado correctamente.",
	"@errorServerError": {
		"description": "HTTP 500"
	}
}
